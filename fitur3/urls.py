from django.conf.urls import url
from .views import *

#url for app
urlpatterns = [
    url(r'^pendaftaranPaket/$', fungsiPendaftaranPaket, name='pendaftaranPaket'),
    url(r'^tambahDariPaket/$', fungsiTambahDariPaket, name='tambahDariPaket'),
    url(r'^regisBeasiswa/$', regisBeasiswa, name='regisBeasiswa'),
    url(r'^aktifkanBeasiswa/$', aktifkanBeasiswa, name='aktifkanBeasiswa'),
]
