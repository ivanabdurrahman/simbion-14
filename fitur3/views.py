from django.shortcuts import render, redirect
from django.db import connection
from django.http import HttpRequest
from datetime import datetime

response = {}
def fungsiPendaftaranPaket(request):
	return render(request, 'pendaftaranPaket.html', response)

def fungsiTambahDariPaket(request):
	cursor = connection.cursor()
	response['username'] = request.session['username']

	cursor.execute("SELECT nomor_identitas FROM donatur WHERE username='" + response['username'] +"'")
	nomor_identitas_donatur = cursor.fetchone()[0]

	cursor.execute("SELECT kode,nama from skema_beasiswa WHERE nomor_identitas_donatur='" +nomor_identitas_donatur +"'")
	response['arrKode'] = cursor.fetchall()

	return render(request, 'tambahDariPaket.html', response)

def regisBeasiswa(request):
	username = request.session['username']

	if request.method == 'POST':
		kode = request.POST['kode']
		nama = request.POST['namapaketbeasiswa']
		jenis = request.POST['jenis']
		desc = request.POST['desc']

		c = connection.cursor()

		c.execute("SELECT nomor_identitas FROM donatur WHERE username='" + username +"'")
		nomor_identitas_donatur = c.fetchone()[0]

		query = "INSERT INTO skema_beasiswa VALUES ('%s', '%s', '%s', '%s', '%s')" %\
		(kode, nama, jenis, desc, nomor_identitas_donatur)
		c.execute(query)
		c.close()

		return redirect('donaturDashboard:index')



def aktifkanBeasiswa(request):
	username = request.session['username']

	if request.method == 'POST':
		kode = request.POST['kode']
		nomorurut= request.POST['nomorurut']
		tglmulaipendaftaran = request.POST['tglmulaipendaftaran']
		tglmulaipenutupan = request.POST['tglmulaipenutupan']
		periode = 'YEARLY'
		status = 'AKTIF'
		print(kode)

		c = connection.cursor()

		# c.execute("SELECT kode FROM skema_beasiswa_aktif WHERE kode='" + kode +"'")
		# kode = c.fetchone()[0]


		query = "INSERT INTO skema_beasiswa_aktif VALUES ('%s', '%s', '%s', '%s', '%s','%s')" %\
		(kode, nomorurut, tglmulaipendaftaran, tglmulaipenutupan, periode, status)

		c.execute(query)
		c.close()

		return redirect('donaturDashboard:index')


	

	return redirect('donaturDashboard:index')
