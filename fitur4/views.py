from django.shortcuts import render, redirect
from django.db import connection
from django.http import HttpRequest
from datetime import datetime


# Create your views here.
response = {}
def index(request):
	cursor = connection.cursor()

	cursor.execute("SELECT DISTINCT S.Nama, SK.tgl_tutup_pendaftaran, SK.status, SK.jumlah_pendaftar, SK.kode_skema_beasiswa, S.jenis, S.nomor_identitas_donatur, S.deskripsi, SB.syarat FROM SKEMA_BEASISWA_AKTIF SK, SKEMA_BEASISWA S, SYARAT_BEASISWA SB WHERE SK.kode_skema_beasiswa=S.kode AND S.kode=SB.kode_beasiswa ")
	hasil = cursor.fetchall()
	response['arrBeasiswa'] = hasil 
	cursor.close()
	return render(request, 'InfoBeasiswa.html', response)