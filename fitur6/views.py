from django.shortcuts import render
from django.db import connection

# Create your views here.
response = {}
def index(request):
	cursor = connection.cursor()

	# dummy username buat sementara
	response['username'] = request.session['username']

	cursor.execute("SELECT role FROM pengguna WHERE username='" + response['username']+"'")
	response['role'] = cursor.fetchone()[0]

	cursor.execute("SELECT nomor_identitas, nama, email, no_telp FROM donatur WHERE username='" + response['username']+"'")
	profil = cursor.fetchone()

	response['nomor_identitas'] = profil[0]
	response['nama'] = profil[1]
	response['email'] = profil[2]
	response['no_telp'] = profil[3]

	cursor.execute(" SELECT distinct nama, tgl_tutup_pendaftaran, status, jumlah_pendaftar, kode FROM skema_beasiswa join skema_beasiswa_aktif on kode=kode_skema_beasiswa WHERE nomor_identitas_donatur='" + response['nomor_identitas']+"'")

	output = cursor.fetchall()
	response['arrBeasiswa'] = output

	return render(request, 'donaturDashboard.html', response)

def detail(request, kodeB):
	cursor = connection.cursor()
	response['username'] = request.session['username']

	cursor.execute(" SELECT distinct nama, deskripsi, kode FROM skema_beasiswa WHERE kode='" + kodeB+"'")
	response['dataBeasiswa'] = cursor.fetchone()

	cursor.execute("SELECT no_urut, nama, npm, waktu_daftar, status_terima FROM pendaftaran NATURAL JOIN mahasiswa WHERE kode_skema_beasiswa = '" + kodeB+"'")
	response['arrPendaftar'] = cursor.fetchall()


	return render(request, 'detailBeasiswa.html', response)

def terima(request, no_urut, kode_beasiswa, npm):
	cursor = connection.cursor()
	cursor.execute("UPDATE pendaftaran SET status_terima='DITERIMA' WHERE no_urut='" +no_urut+ "' AND kode_skema_beasiswa='" +kode_beasiswa+ "' AND npm='" +npm+ "'")

	return detail(request, kode_beasiswa)


def tolak(request, no_urut, kode_beasiswa, npm):
	cursor = connection.cursor()
	cursor.execute("UPDATE pendaftaran SET status_terima='DITOLAK' WHERE no_urut='" +no_urut+ "' AND kode_skema_beasiswa='" +kode_beasiswa+ "' AND npm='" +npm+ "'")

	return detail(request, kode_beasiswa)
