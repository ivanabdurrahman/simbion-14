from django.conf.urls import url
from .views import *
#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^detail/(?P<kodeB>.*)/', detail, name='detail' ),
    url(r'^terima/(?P<no_urut>.*)/(?P<kode_beasiswa>.*)/(?P<npm>.*)/', terima, name='terima' ),
    url(r'^tolak/(?P<no_urut>.*)/(?P<kode_beasiswa>.*)/(?P<npm>.*)/', tolak, name='tolak' ),
]
