"""projSIMBION URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from homepage.views import index as index_homepage
import homepage.urls as homepage
import fitur1.urls as fitur1
import fitur2.urls as fitur2
import fitur3.urls as fitur3
import fitur4.urls as fitur4
import fitur5.urls as fitur5
import fitur6.urls as fitur6

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^home/', include(homepage,namespace='homepage')),
    url(r'^$', index_homepage, name='index'),
    # url(r'^pendaftaranPaket/', include(fitur3,namespace='pendaftaranPaket')),
    # url(r'^tambahDariPaket/', include(fitur3,namespace='tambahDariPaket')),
    url(r'^auth/', include(fitur1,namespace='login')),
    url(r'^register/', include(fitur2,namespace='register')),
    url(r'^PaketBeasiswa/', include(fitur3,namespace='tambahDariPaket')),
    url(r'^infoBeasiswa/', include(fitur4,namespace='infoBeasiswa')),
    url(r'^mahasiswaDashboard/', include(fitur5,namespace='mahasiswaDashboard')),
    url(r'^donaturDashboard/', include(fitur6,namespace='donaturDashboard')),
]
