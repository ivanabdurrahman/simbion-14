from django.conf.urls import url
from .views import *

urlpatterns = [
	url(r'^$', index, name = 'index'),
	url(r'mahasiswa/$', regMahasiswa, name = 'regMahasiswa'),
	url(r'individual-donatur/$', regDonIndividu, name = 'regDonIndividu'),
	url(r'yayasan/$', regYayasan, name = 'regYayasan'),
	url(r'addMahasiswa/$', add_mahasiswa, name = 'addMahasiswa'),
	url(r'addDonaturYayasan/$', add_donatur_yayasan, name='addDonaturYayasan'),
	url(r'addDonaturIndividual/$', add_donatur_individual, name='addDonaturIndividual')
]