from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.urls import reverse
from django.db import connection
import os
import json

response = {}

def index (request):
    return render (request, 'register.html', response)

def regMahasiswa(request):
    return render (request, 'registerMahasiswa.html', response)

def regDonIndividu(request):
    return render (request, 'registerDonIndividu.html', response)

def regYayasan(request):
    return render (request, 'registerYayasan.html', response)

@csrf_exempt
def add_mahasiswa (request):
    if request.method == 'POST':
        c = connection.cursor()

        username = request.POST['username']
        password = request.POST['password']
        npm = request.POST['npm']
        email = request.POST['email']
        nama = request.POST['nama']
        no_telp = request.POST['no_telp']
        alamat_tinggal = request.POST['alamat_tinggal']
        alamat_domisili = request.POST['alamat_domisili']
        nama_bank = request.POST['nama_bank']
        no_rekening = request.POST['no_rekening']
        nama_pemilik = request.POST['nama_pemilik']

        c.execute('SELECT username FROM pengguna')
        daftarPengguna =  c.fetchall()
        c.execute('SELECT npm FROM mahasiswa')
        daftarMahasiswa = c.fetchall()

        penanda_username = False
        penanda_npm = False

        for uname in daftarPengguna :
            if (uname[0] == username):
                penanda_username = True
        for nomor in daftarMahasiswa :
            if (nomor[0] == npm):
                penanda_npm = True



        penanda_angka = False
        penanda_panjang = False

        for huruf in password :
            if (huruf.isdigit()):
                penanda_angka = True
        if len(password)>7 :
            penanda_panjang = True

        penanda_password = penanda_panjang and penanda_angka


        print(penanda_username)
        print(penanda_password)
        # if (not penanda_username) and penanda_password :
            
        query = "INSERT INTO pengguna VALUES ('%s', '%s', 'mahasiswa')" %\
        (username, password)
        c.execute(query)

        # if (not penanda_npm):
        query = "INSERT INTO mahasiswa VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')" %\
        (npm, email, nama, no_telp, alamat_tinggal, alamat_domisili, nama_bank, no_rekening, nama_pemilik, username)
        c.execute(query)

        c.close()

        request.session['username'] = username
        request.session['role'] = "mahasiswa"
        request.session['is_login'] = "login"

        return HttpResponseRedirect(reverse('mahasiswaDashboard:index'))


@csrf_exempt
def add_donatur_individual(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        nomor_identitas = request.POST['nomor_identitas']
        nik = request.POST['nik']
        email = request.POST['email']
        nama = request.POST['nama']
        npwp = request.POST['npwp']
        no_telp = request.POST['no_telp']
        alamat = request.POST['alamat']

        c = connection.cursor()

        #CEK USERNAME BELUM TERDAFTAR
        c.execute('SELECT username FROM pengguna')
        daftarPengguna = c.fetchall()

        penanda_username = False

        for uname in daftarPengguna :
            if (uname[0] == username):
                penanda_username = True

        c.execute('SELECT nomor_identitas FROM donatur')
        daftarNomorIdentitas = c.fetchall()
        penanda_nomor_identitas = False

        for nomor in daftarNomorIdentitas :
            if (nomor[0] == nomor_identitas):
                penanda_nomor_identitas = True

        #Check that password have minimal eight character and any digit there
        penanda_angka = False
        penanda_panjang = False

        for huruf in password :
            if (huruf.isdigit()):
                penanda_angka = True
        if len(password)>7 :
            penanda_panjang = True

        penanda_password = penanda_panjang and penanda_angka

        # if (not penanda_username) and penanda_password and (not penanda_nomor_identitas) :
        query = "INSERT INTO pengguna VALUES ('%s', '%s', 'donatur')" %\
        (username, password)
        c.execute(query)

        #Cek Apakah nomor Identitas Donatur sudah terdaftar
        #Cek apakah nik donatur sudah terdaftar
        #Cek apakah jumlah karakter NIK donatur adalah tepat 16 buah

        c.execute('SELECT nik FROM individual_donor')
        daftarNIK = c.fetchall()

        penanda_nik = False
        penanda_nik_panjang = False

        for nomor in daftarNIK :
        	if (nomor[0] == nik):
        		penanda_nik = True
        if len(nik)==16:
        	penanda_nik_panjang = True

        # if (not penanda_nik and penanda_nik_panjang ):
        query = "INSERT INTO donatur VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s')" %\
        (nomor_identitas, email, nama, npwp, no_telp, alamat, username)
        query2 = "INSERT INTO individual_donor VALUES ('%s', '%s')" %\
        (nik, nomor_identitas)
        c.execute(query)
        c.execute(query2)

        request.session['username'] = username
        request.session['role'] = "donatur"
        request.session['is_login'] = "login"

        c.close()

        return HttpResponseRedirect(reverse('donaturDashboard:index'))

@csrf_exempt
def add_donatur_yayasan (request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        nomor_identitas = request.POST['nomor_identitas']
        nomor_sk_yayasan = request.POST['nomor_sk_yayasan']
        email = request.POST['email']
        nama = request.POST['nama']
        no_telp = request.POST['no_telp']
        npwp = request.POST['npwp']
        alamat = request.POST['alamat']

        c = connection.cursor()

        #CEK USERNAME BELUM TERDAFTAR
        #Cek jumlah karakter pada password adalah minimal 8 karakter
        c.execute('SELECT username FROM pengguna')
        daftarPengguna = c.fetchall()

        penanda_username = False

        for uname in daftarPengguna :
            if (uname[0] == username):
                penanda_username = True

        c.execute('SELECT nomor_identitas FROM donatur')
        daftarNomorIdentitas = c.fetchall()
        penanda_nomor_identitas = False

        for nomor in daftarNomorIdentitas :
            if (nomor[0] == nomor_identitas):
                penanda_nomor_identitas = True

        #Check that password have minimal eight character and any digit there
        penanda_angka = False
        penanda_panjang = False

        for huruf in password :
            if (huruf.isdigit()):
                penanda_angka = True
        if len(password)>7 :
            penanda_panjang = True

        penanda_password = penanda_panjang and penanda_angka

        # if (not penanda_username) and penanda_password and (not penanda_nomor_identitas) :
        query = "INSERT INTO pengguna VALUES ('%s', '%s', 'donatur')" %\
        (username, password)
        c.execute(query)

        c.execute('SELECT no_sk_yayasan FROM yayasan')
        daftar_nomor_SK_yayasan = c.fetchall()

        penanda_SK = False

        for nomor in daftar_nomor_SK_yayasan :
        	if (nomor[0] == daftar_nomor_SK_yayasan):
        		penanda_SK = True

        # if (not penanda_SK):
        query = "INSERT INTO donatur VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s')" %\
        (nomor_identitas, email, nama, npwp, no_telp, alamat, username)
        query2 = "INSERT INTO yayasan VALUES ('%s', '%s', '%s', '%s', '%s')" %\
        (nomor_sk_yayasan, email, nama, no_telp, nomor_identitas)
        c.execute(query)
        c.execute(query2)

        request.session['username'] = username
        request.session['role'] = "donatur"
        request.session['is_login'] = "login"


        c.close()
        return HttpResponseRedirect(reverse('donaturDashboard:index'))
