from django.conf.urls import url
from .views import index, formDaftarBeasiswa, daftar
#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^daftarBeasiswa/', formDaftarBeasiswa, name='formDaftarBeasiswa'),
    url(r'^daftar/', daftar, name='daftar'),
]
