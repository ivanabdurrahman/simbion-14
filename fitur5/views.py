from django.shortcuts import render, redirect
from django.db import connection
from django.http import HttpRequest
from datetime import datetime

# Create your views here.
response = {}
def index(request):
	cursor = connection.cursor()
	# dummy username buat sementara
	response['username'] = request.session['username']

	cursor.execute("SELECT role FROM pengguna WHERE username='" + response['username']+"'")
	response['role'] = cursor.fetchone()[0]

	cursor.execute("SELECT npm, nama, email, no_telp FROM mahasiswa WHERE username='" + response['username']+"'")
	profil = cursor.fetchone()

	response['npm'] = profil[0]
	response['nama'] = profil[1]
	response['email'] = profil[2]
	response['no_telp'] = profil[3]


	cursor.execute("SELECT no_urut, nama, tgl_tutup_pendaftaran, status_daftar FROM pendaftaran p NATURAL JOIN skema_beasiswa_aktif a JOIN skema_beasiswa s ON kode=a.kode_skema_beasiswa WHERE npm = '" + response['npm']+"'")
	output = cursor.fetchall()
	response['arrBeasiswa'] = output

	cursor.close()

	return render(request, 'mahasiswaDashboard.html', response)

def formDaftarBeasiswa(request):
	cursor = connection.cursor()

	cursor.execute("SELECT kode, nama, tgl_mulai_pendaftaran from skema_beasiswa_aktif JOIN skema_beasiswa ON kode_skema_beasiswa = kode WHERE status = 'TRUE' ORDER BY kode")
	response['arrBeasiswaAktif'] = cursor.fetchall()

	return render(request, 'formDaftarBeasiswa.html', response)

def daftar(request):
	if request.method == 'POST':
		cursor = connection.cursor()
		# dummy username buat sementara
		response['username'] = request.session['username']

		cursor.execute("SELECT no_urut from skema_beasiswa_aktif WHERE kode_skema_beasiswa='" + request.POST['kode'] + "'")
		no_urut_baru = cursor.fetchone()[0]

		cursor.execute("SELECT npm FROM mahasiswa WHERE username='" + response['username'] + "'")
		npm = cursor.fetchone()[0]
		waktu_daftar = str(datetime.now())


		cursor.execute("INSERT INTO pendaftaran VALUES ('"+ str(no_urut_baru)+ "','" +request.POST['kode']+ "','"+ npm +"','" +waktu_daftar+ "', 'pendaftaran ok', 'waiting')")

		return redirect('mahasiswaDashboard:index')
