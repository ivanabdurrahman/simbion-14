from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.urls import reverse
from django.db import connection
import os
import json

# Create your views here.
response = {}
def index(request):
    return render(request, 'login&logout.html', response)
    
def login(request):
    if request.method == 'POST':
        c = connection.cursor()

        username = request.POST['username']
        password = request.POST['password']

        c.execute('SELECT username FROM pengguna')
        daftarPengguna =  c.fetchall()

        penanda_username = False

        for uname in daftarPengguna :
            if (uname[0] == username):
                penanda_username = True

        c.execute("SELECT password, role FROM pengguna WHERE username = '" + username + "'")
        data =  c.fetchone()
        passwordUser = data[0]
        role = data[1]

        if (password==passwordUser) :
            request.session['username'] = username
            request.session['role'] = role
            request.session['is_login'] = "login"

            if (role=='mahasiswa'):
                return redirect(reverse('mahasiswaDashboard:index'))


            elif (role=='donatur'):
                return redirect(reverse('donaturDashboard:index'))

        c.close()

        return redirect(reverse('homepage:index'))

def logout(request):
    try:
        request.session.flush()
    except KeyError:
        pass
    return redirect(reverse('homepage:index'))
